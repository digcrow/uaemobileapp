/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import { Root, Button } from "native-base";
import { AppLoading } from 'expo';
import { StackNavigator, DrawerNavigator } from "react-navigation";
import MainHeader from './app/compoents/header';
import cacheAssetsAsync from './app/lib/cacheAssetsAsync';

// Screens = App Pages
import Home from "./app/screens/home";
import Landing from "./app/screens/landing";
import ServicesList from "./app/screens/servicesList";
import DrawerMenu from "./app/screens/drawer";

const Drawer = DrawerNavigator(
    {
        Home: { screen: Home },
        Anatomy: { screen: ServicesList },
    },
    {
        initialRouteName: "Home",
        contentOptions: {
            activeTintColor: "#e91e63"
        },
        contentComponent: props => <DrawerMenu {...props} />
    }
);

const AppNavigator = StackNavigator(
    {
        Drawer: { screen: Drawer },
        Landing:{
            name: 'Landing page',
            screen: Landing,
        },
        Home: {
            name: 'Homepage',
            screen: Home,
            navigationOptions: {
                gesturesEnabled: false
            }
        },
        ServicesList: {
            name: 'ServicesList',
            screen: ServicesList,
        }
    },
    {
        initialRouteName: "Landing",
        // initialRouteName: "Drawer",
        headerMode: 'screen',
        tabBarPosition: 'top',
        animationEnabled: true,
        navigationOptions: ({ navigation }) => ({
            header:  ({ navigation }) => <MainHeader navigation={navigation} />
        })
    }
);



export default class App extends React.Component {
    state = {
        appIsReady: false,
        appLang: null
    };
    componentWillMount() {
        this._loadAssetsAsync();
    }
    async _loadAssetsAsync() {
        try {
            await cacheAssetsAsync({
                fonts: [
                    { 'Cairo': require('./app/assets/fonts/Cairo/Cairo-Regular.ttf') },
                    { 'Cairo-SemiBold': require('./app/assets/fonts/Cairo/Cairo-SemiBold.ttf') },
                    { 'Cairo-Bold': require('./app/assets/fonts/Cairo/Cairo-Bold.ttf') },
                ],
            });
        } catch (e) {
            console.warn(
                'There was an error caching assets (see: main.js), perhaps due to a ' +
                'network timeout, so we skipped caching. Reload the app to try again.'
            );
            console.log(e.message);
        } finally {
            this.setState({ appIsReady: true });
        }
    }

    render() {
        if (this.state.appIsReady) {
            return(
                <Root>
                    <AppNavigator />
                </Root>
            )
        } else {
            return <AppLoading />;
        }
    }
}
