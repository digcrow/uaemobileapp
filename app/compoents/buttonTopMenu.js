import React, {Component} from 'react';
import {TouchableWithoutFeedback, Image, View, StyleSheet} from 'react-native';
import {colors} from "../config/styles";


class ButtonTopMenu extends Component {
    render() {
        let icon =  null;
        switch (this.props.icon) {
            case 'back':
                icon = require('../assets/images/Icones/Retour.png');
                break;
            case 'services':
                icon = require('../assets/images/Icones/Services.png');
                break;
            case 'chat':
                icon = require('../assets/images/Icones/Chat.png');
                break;
            case 'satisfaction':
                icon = require('../assets/images/Icones/Satisfaction.png');
                break;
            case 'menu':
                icon = require('../assets/images/Icones/Menu.png');
                break;
        }
        return (
            <TouchableWithoutFeedback onPress={this.props.fn}>
                <View style={styles.iconcontainer}>
                    <Image
                        style={styles.imageicon}
                        source={icon}
                    />
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

const styles = StyleSheet.create({
    iconcontainer: {
        width: 30,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10,
    },
        imageicon: {
            width: 30,
            height: 30,
            resizeMode: 'contain',
        },
    header: {
        backgroundColor: colors.red
    }
});

export default ButtonTopMenu;