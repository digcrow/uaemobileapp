import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {Header, Right, Left} from 'native-base';
import ButtonTopMenu from '../compoents/buttonTopMenu'
import {colors} from "../config/styles";

let backButton, navigation;

class MainHeader extends Component {

    constructor(props) {
        super(props);
        this.state = {activeScreen: 'landing'};
    }

    findRouteNameFromNavigatorState ({ routes }) {
        let route = routes[routes.length - 1];
        while (route.index !== undefined) route = route.routes[route.index];
        return route.routeName;
    }

    navigateToScreen(screenName) {

        const navigation = this.props.navigation;
        const currentScreen = navigation.state.routes[navigation.state.index];
        if (currentScreen.routeName !== screenName) {
            this.setState({activeScreen:screenName});
            return navigation.navigate('ServicesList');
        }
    }

    componentWillUpdate() {
        navigation = this.props.navigation;
        let currentPage = this.findRouteNameFromNavigatorState(navigation.state);

        console.log(currentPage);

        if( currentPage === 'ServicesList' )
            styles.header = { backgroundColor : colors.gold };

        if( currentPage === 'Home' )
            styles.header = { backgroundColor : colors.red };

        backButton = null;
        if ( currentPage !== "Home" ) {
            backButton = <ButtonTopMenu fn={() => navigation.goBack()} icon={'back'}/>
        }
    }

    render() {
        return (
            <Header style={styles.header}
                    iosBarStyle={'light-content'}
                    hasSegment={false}
                    transparent>
                <Left>
                    {backButton}
                </Left>
                <Right>
                    <ButtonTopMenu fn={() => this.navigateToScreen('ServicesList')} icon={'services'}/>
                    <ButtonTopMenu fn={() => this.navigateToScreen('ServicesList')} icon={'chat'}/>
                    <ButtonTopMenu fn={() => this.navigateToScreen('ServicesList')} icon={'satisfaction'}/>
                    <ButtonTopMenu fn={() => this.props.navigation.navigate("DrawerOpen") } icon={'menu'}/>
                </Right>
            </Header>
        )
    }
}


const styles = StyleSheet.create({
    header: {
        backgroundColor: colors.red,
        elevation: 0,
        shadowOffset: {height: 0, width: 0},
        shadowOpacity: 0,
    }
});

export default MainHeader;