import React, {Component} from 'react';
import {TouchableOpacity, Image, ImageBackground, View, TextInput,I18nManager} from 'react-native';
import {Container, Title, Content, Button, Card, List, ListItem, Thumbnail, Body, Text} from 'native-base';
// import LinearGradient from 'react-native-linear-gradient';
/*<LinearGradient colors={['#FFF', '#000']} style={styles.linearGradient}>*/

// import I18n from '../../locales/i18n';
import { pageTitle, pageHeader } from '../../config/styles';

import styles from './styles';
import I18n from "../../locales/i18n";


export default class Home extends Component {

    _onDirectionChange() {
        I18nManager.forceRTL(!this.state.isRTL)

        this.setState({
            isRTL: !this.state.isRTL
        })
    }

    openPage(page){

        console.log('-> Opening Page : '+  I18nManager.isRTL);
    }
    openService(service){
        console.log('-> Service Page : '+ service);
    }

    render() {
        const {navigate, state} = this.props.navigation;
        return (

            <Container style={styles.container}>
                <Content padder>
                    {/*<LinearGradient colors={['#D10004', '#710000']} style={styles.linearGradient}>*/}
                    {/*</LinearGradient>*/}
                    <View style={pageHeader}>
                        <Title style={pageTitle}>{I18n.t('home.headerTitle')}</Title>
                    </View>

                    <TouchableOpacity activeOpacity={0.8} onPress={()=> this.openPage('presentaion')}>
                        <View style={styles.cardItem}>
                            <View style={styles.cardItemInner}>
                                <View style={styles.cardTitleWrapper}>
                                    <Text style={styles.cardTitle}>{I18n.t('home.presentation.title')}</Text>
                                </View>
                                <Image
                                    style={styles.bgCards}
                                    source={require('../../assets/images/Home/Presentation.png')}
                                />
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.8} onPress={()=> this.openPage('yearOfZied')}>
                        <View style={styles.cardItem}>
                            <View style={styles.cardItemInner}>
                                <View style={styles.cardTitleWrapper}>
                                    <Text style={styles.cardTitle}>{I18n.t('home.yearZayed.title')}</Text>
                                </View>
                                <Image
                                    style={styles.bgCards}
                                    source={require('../../assets/images/Home/Year-Zayed.png')}
                                />
                            </View>
                        </View>
                    </TouchableOpacity>

                    <View style={styles.cardItem}>
                        <View style={styles.cardItemInner}>
                            <View style={styles.cardTitleWrapper}>
                                <Text style={styles.cardTitleWhite}>{I18n.t('home.tracking.title')}</Text>
                                <Text style={styles.cardDesWhite}>{I18n.t('home.tracking.des')}</Text>
                                <View style={styles.inputText}>
                                    <TextInput
                                        style={styles.textInput}
                                        underlineColorAndroid={'white'}
                                        placeholder={I18n.t('home.tracking.placeholder')}
                                        placeholderTextColor={'gray'}
                                        maxLength={10}
                                    />
                                </View>
                                <TouchableOpacity activeOpacity={0.8}>
                                    <Button style={styles.sendBtn} onPress={()=> this.openPage('tracking')}>
                                        <Text style={styles.sendBtnTxt}> {I18n.t('home.tracking.btnSubmit')} <Image  style={styles.sendBtnImage} source={require('../../assets/images/Home/send-btn.png')}/></Text>
                                    </Button>
                                </TouchableOpacity>
                            </View>
                            <Image
                                style={styles.bgCards}
                                source={require('../../assets/images/Home/Request-Tracking.png')}
                            />
                        </View>
                    </View>
                    <View style={styles.cardItemTall}>
                        <View style={styles.cardItemTallInner}>
                            <View style={styles.cardTitleWrapper}>
                                <Text style={styles.cardTitle}>{I18n.t('home.topServices.title')}</Text>
                                <Content>
                                    <List>
                                        <ListItem onPress={()=> this.openService('visa')}>
                                            <Thumbnail square size={80} source={require('../../assets/images/Services/Visa.png')} />
                                            <Body>
                                                <Text style={styles.topServiceAlign}>{I18n.t('home.topServices.services.visa.title')}</Text>
                                                <Text style={styles.topServiceAlign} note>{I18n.t('home.topServices.services.visa.des')}</Text>
                                            </Body>
                                        </ListItem>
                                        <ListItem onPress={()=> this.openService('tawajudi')}>
                                            <Thumbnail square size={80} source={require('../../assets/images/Services/Tawajudi.png')} />
                                            <Body>
                                                <Text style={styles.topServiceAlign}>{I18n.t('home.topServices.services.tawajudi.title')}</Text>
                                                <Text style={styles.topServiceAlign} note>{I18n.t('home.topServices.services.tawajudi.des')}</Text>
                                            </Body>
                                        </ListItem>
                                        <ListItem onPress={()=> this.openService('shortVisa')}>
                                            <Thumbnail square size={80} source={require('../../assets/images/Services/Short.png')} />
                                            <Body>
                                                <Text style={styles.topServiceAlign}>{I18n.t('home.topServices.services.visa.title')}</Text>
                                                <Text style={styles.topServiceAlign} note>{I18n.t('home.topServices.services.visa.des')}</Text>
                                            </Body>
                                        </ListItem>
                                    </List>
                                </Content>
                            </View>
                        </View>
                    </View>

                    <TouchableOpacity activeOpacity={0.8} onPress={()=> this.openPage('video')}>
                        <View style={styles.cardItem} onPress={()=> console.log('asdasdsa')}>
                            <View style={styles.cardItemInner}>
                                <View style={styles.cardTitleWrapper}>
                                    <Text style={styles.cardTitle}>{I18n.t('home.video.title')}</Text>
                                </View>
                                <Image
                                    style={styles.bgCards}
                                    source={require('../../assets/images/Home/Video.png')}
                                />
                            </View>
                        </View>
                    </TouchableOpacity>



                </Content>
            </Container>

        );
    }
}