import { StyleSheet,I18nManager } from 'react-native';
import { colors } from "../../config/styles";

export default styles = StyleSheet.create({
    container : {
        backgroundColor: colors.red
    },
    cardItem : {
        borderRadius: 10,
        height: 230,
        backgroundColor: '#FFF',
        position: 'relative',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 3,
        marginBottom: 20,
    },
    cardItemTall: {
        borderRadius: 10,
        backgroundColor: '#FFF',
        position: 'relative',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 3,
        marginBottom: 20,
        height: 320,
    },
    cardItemInner : {
        overflow: 'hidden',
        height: 230,
        borderRadius: 10,
    },
    cardItemTallInner: {
        overflow: 'hidden',
        height: 310,
        borderRadius: 10,
    },

    bgCards: {
        width: '110%',
        height: '105%',
        marginLeft: '-5%',
        marginTop: '-2%',
        resizeMode: 'contain'
    },
    cardTitleWrapper : {
        padding: 15,
        position: 'absolute',
        top : 0,
        width: '100%',
        height: '100%',
        zIndex: 1
    },
    cardTitle: {
        fontSize: 26,
        fontWeight: 'bold',
        marginBottom: 10,
        color: '#000',
        textAlign:  'left'
    },
    cardTitleWhite: {
        fontSize: 26,
        fontWeight: 'bold',
        marginBottom: 10,
        color: '#FFF',
        textAlign:  'left'
    },
    cardDesWhite: {
        color: '#FFF',
        marginBottom: 10,
        textAlign:  'left'
    },
    sendBtn: {
        backgroundColor: 'transparent',
        position: 'absolute',
        right: 0,
    },
    textInput: {
        backgroundColor: '#FFF',
        borderRadius: 9,
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 10,
        paddingBottom: 10,
        height: 50,
        marginTop: 20,
        marginBottom: 10,
        fontSize: 15,
    },
    sendBtnTxt: {
        color: '#FFF',
        fontSize: 15,
        fontWeight: '700',
    },
    sendBtnImage: {
        marginTop: 2,
        marginLeft: 20
    },
    topServiceAlign: {
        textAlign:  'left'
    }
});