import React, { Component } from 'react';
import { Image, ImageBackground, I18nManager } from 'react-native';
import { Container, View, Button, Text } from 'native-base';
import I18n from '../../locales/i18n';

import styles from './styles';

export default class Landing extends Component {

    static navigationOptions = {
        header: null
    };

    render() {
        const { navigate } = this.props.navigation;

        return (
            <ImageBackground
                style={styles.bgContainer}
                source={require('../../assets/images/bg-landing.png')}
            >
                <Container style={styles.container}>
                    <View style={styles.mainContainer}>
                        <View style={styles.topView}>
                            <Image
                                style={styles.mainLogo}
                                source={require('../../assets/images/main-logo.png')}
                            />
                            <Text style={styles.title}>
                                سفارة دولة الإمارات بتونس
                            </Text>
                            <Text style={styles.subTitle}>
                                القسم القنصلي - مركز التأشيرات
                            </Text>
                        </View>

                        <View style={styles.sloganWrapper}>
                            <Image
                                style={styles.flag}
                                source={require('../../assets/images/uae-flag.png')}
                            />
                            <Text style={styles.slogan}>التميز … الدقة … توفير الوقت</Text>
                        </View>


                        <View  style={styles.botView}>
                            <Image
                                style={styles.bgLine}
                                source={require('../../assets/images/line-select-lang.png')}
                            />
                            <Text style={styles.selectLang}>
                                الرجاء اختيار اللّغة

                            </Text>
                            <View style={styles.btnWrapper}>
                                <Button
                                    style={styles.btnOutline}
                                    onPress={ () => {
                                        I18n.locale = 'en';
                                        I18nManager.forceRTL(false);
                                        navigate("Home")
                                    }}
                                >
                                    <Text style={styles.textBtn}>English</Text>
                                </Button>
                                <Button
                                    style={styles.btnOutline}
                                    onPress={ () => {
                                        I18n.locale = 'ar';
                                        I18nManager.forceRTL(true);
                                        navigate("Home")
                                    }}
                                >
                                    <Text style={[styles.textBtn, styles.fontAr]}>عربية</Text>
                                </Button>
                            </View>
                        </View>

                           <Text style={styles.copyright}>V1.0 © Copyright 2018</Text>

                    </View>
                </Container>
            </ImageBackground>
        );
    }
}