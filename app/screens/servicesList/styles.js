import { StyleSheet } from 'react-native';
import { colors } from "../../config/styles";

export default styles = StyleSheet.create({
    iconcontainer: {
        width: 30,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10,
    },
        imageicon: {
            width: 30,
            height: 30,
            resizeMode: 'contain',
        },
    header: {
        backgroundColor: colors.red
    },
    mainContainer: {
        width: '100%',
        height: '100%',
        padding: 20,
        alignItems: 'center',
        justifyContent: 'space-between',

    },
    mainLogo: {
        width: 90,
        height: 90,
        marginTop: 60,
        marginBottom: 20
    },

    botView: {
        width: '100%',
        alignItems: 'center',
    },
    topView: {
        width: '100%',
        alignItems: 'center',
    },
    title: {
        color: '#FFF',
        fontSize: 22,
        marginBottom: 5,
        fontFamily: 'Cairo-SemiBold',
    },
    subTitle: {
        color: colors.gold,
        fontSize: 14
    },


    sloganWrapper: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 50
    },
    flag: {
        width: 32,
        height: 21,
        marginBottom: 10
    },
    slogan: {
        color: '#FFF',
        fontSize: 22,
        fontFamily: 'Cairo-SemiBold',
    },

    bgLine: {
        width: '100%',
        alignItems: 'center',
        opacity: 0.3,
        position: 'absolute',
        top: 20
    },
    selectLang:{
        color: '#FFF',
        fontFamily: 'Cairo-SemiBold',
        fontSize: 16,
        marginBottom: 40
    },

    btnWrapper : {
        width: '73%',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    btnOutline : {
        width: 110,
        height: 35,
        borderRadius: 18,
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#fff",
        backgroundColor: 'transparent'
    },
    textBtn: {
        width: '100%',
        textAlign : 'center',
        fontSize: 15,
        lineHeight: 35
    },


    fontAr: {
        fontFamily: 'Cairo'
    },
    copyright: {
        fontSize: 12,
        color: '#FFF',
        opacity: 0.6,
    }
});