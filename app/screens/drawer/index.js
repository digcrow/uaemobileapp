import React, {Component} from 'react';
import {Drawer} from 'native-base';
import MainMenu from '../../compoents/menu'


export default class DrawerMenu extends Component {
    closeDrawer = () => {
        this.drawer._root.close()
    };
    openDrawer = () => {
        this.drawer._root.open()
    };
    render() {
        return (
            <Drawer
                ref={(ref) => { this.drawer = ref; }}
                content={ <MainMenu navigator={this.navigator} /> }
                onClose={() => this.closeDrawer()} >
            </Drawer>
        );
    }
}
