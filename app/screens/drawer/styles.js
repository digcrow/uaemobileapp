import { StyleSheet } from 'react-native';
import { colors } from "../../config/styles";

export default styles = StyleSheet.create({
    container : {
        backgroundColor: colors.red
    },
    cardItem : {
        borderRadius: 10,
        overflow: 'hidden',
        height: 250,
        backgroundColor: '#000'
    },
    bgCards: {
        width: '110%',
        height: '110%',
        marginLeft: -6,
        marginTop: -5
    },
    cardItemView : {
        padding: 20
    },
    cardTitle: {
        fontSize: 22,
        fontWeight: 'bold'
    }
});