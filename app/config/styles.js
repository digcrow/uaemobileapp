// Global Style

export const colors = {
    red : '#cb0103',
    gold : '#cb9a40',
    grey : '#8B8B85',
    services : {
        workVisa : ['#006898'],
        citizens : ['#19b496'],
        indivComp : ['#86610e'],
        pDtermina : ['#b50e0e']
    }
};

export const fonts = {
    ar : 'arabicFont',
    latin : 'latinFont'
};

export const pageTitle = {
    fontSize: 28,
    color: '#FFF',
    marginBottom: 5,
    textAlign: 'left',
};

export const pageHeader = {
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(255, 255, 255, 0.3)',
    marginBottom: 20,
};
